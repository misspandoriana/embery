(function($) {
    $.totop = function(element, options){
        var defaults = {};
        
        var $element = $(element),
        element = element;
        var totopBtn = $('.kris-totop');
        

        $element.init = function(){     
            this.settings = $.extend({}, defaults, options); 
            topPosition();
            totopBtn.bind('click', clickTotopBtn);



            
        };

        var topPosition = function(){
            var boxH = $('.kris-totop').height();

            var winH = $(window).height();

            var margT = (winH - boxH)/2;

            if(margT < 0){margT = 0;}

            totopBtn.css({'top': margT + 'px'});
        };

        var clickTotopBtn = function(){
            var scTop = $("body").scrollTop();
            if(scTop > 0){
                $("html,body").stop().animate({ 
                    scrollTop: 0
                }, 'normal'); 
            }
        };

        $(window).resize(function(){
            topPosition();
        });

        $(window).scroll(function(){
            var currentScrollTop = $(window).scrollTop();
            //console.log('currentScrollTop = ' + currentScrollTop);
            if(currentScrollTop > 0){
               totopBtn.fadeIn(); 
            }else{
                totopBtn.fadeOut(); 
            }
        })

        
        $element.init();
    }

    $.fn.totop = function(options){
        return this.each(function(){
            if (undefined == $(this).data('totop')){
                var plugin = new $.totop(this, options);
                $(this).data('totop', plugin);
            }
        });
    }
})(jQuery);























