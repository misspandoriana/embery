(function($) {
    $.detailinfo = function(element, options){

        var defaults = {};
        
        var $element = $(element),
        element = element;

        var bigImgBox = $('#info-about-pic');
        var headerSite = $('.kris-page-head');
        var btnL = $('.showMoreInfo-btn-l');
        var btnR = $('.showMoreInfo-btn-r');
        var currentSmImg;

        
        var currElParrentLine;

        if(isOpen !== undefined){
            var currentOpen = isOpen;
        }else{
            var currentOpen;
        }
        



        $element.init = function(){     
            this.settings = $.extend({}, defaults, options); 

            $.each($('.kris-coll-box'), function(id, el){
                $(el).live('click', funcStart.bigImgBoxPlacer);
            });
            btnL.live('click', funcStart.clickBtnL);
            btnR.live('click', funcStart.clickBtnR);

        };

        var funcStart = {
            bigImgBoxPlacer: function(){
                var el = $(this);
                currentSmImg = el.find('.kris-coll-img img').attr('src');
                var lengCollBox = $('.kris-coll-box').length - 1;
                currentOpen = $('.kris-coll-box').index(this);
                if(currentOpen == 0){
                    btnL.hide();
                    btnR.show();
                }else if(currentOpen == lengCollBox){
                    btnL.show();
                    btnR.hide();
                }else{
                    btnL.show();
                    btnR.show();
                };

                
                $('.kris-coll-box').removeClass('active');
                el.addClass('active');
                var elParrentLine = el.parent().get(0);
                if(currElParrentLine != elParrentLine){
                    console.log('elParrentLine = ' + elParrentLine);
                    bigImgBox.detach().prependTo(elParrentLine);
                    currElParrentLine = elParrentLine;
                }else{
                    console.log('in normal place');
                };

                if(!bigImgBox.is(':visible')){
                    bigImgBox.slideDown('slow');
                }else{
                    console.log('bigImgBox is visible');
                };

                var targetOffset = bigImgBox.offset().top - headerSite.height();
                console.log('targetOffset = ' + targetOffset);
                $("html,body").stop().animate({ 
                    scrollTop: targetOffset
                }, 1100); 
                
                var objAbout = el.data('about');
                funcStart.insertInfo(el, objAbout);
            },
            insertInfo: function(el, objAbout){
                var newLink = window.location.pathname + '?id=' + objAbout.id;
                History.pushState({}, null, newLink);
                bigImgBox.find('.insertId').html(objAbout.id);
                bigImgBox.find('.insertName').html(objAbout.name);
                bigImgBox.find('.insertAutor').html(objAbout.autor);
                bigImgBox.find('.insertPrice').html(objAbout.price);
                bigImgBox.find('.insertTxt').html(objAbout.txt);

                if($('.kris-coll-box.'+objAbout.id+'').data('about.fav') == true){
                    $('.imgFav').addClass('active');
                    $('.txtFav').html('В избранном&nbsp;&nbsp;<span class="favDell" style="font-size: 16px; text-decoration: underline;">Удалить</span>');
                    $('.addlike').data('isFav', true);
                }else if(objAbout.fav == false){
                    $('.imgFav').removeClass('active');
                    $('.txtFav').html('Добавить в избранное');
                    $('.addlike').data('isFav', false);
                }


                var tegs = objAbout.tegs;
                var liStrTeg = '<li class="blue">Тэги:</li>&nbsp;'
                var lngTeg = tegs.length - 1;
                jQuery.each(tegs, function(i, val){
                    if(i != lngTeg){
                        liStrTeg = liStrTeg + '<li><a href="' + this[1] + '">' + this[0] + ', </a></li>'
                    }else{
                        liStrTeg = liStrTeg + '<li><a href="' + this[1] + '">' + this[0] + '</a></li>'
                    }
                 });
                //bigImgBox.find('.insertTegs').html(liStrTeg);

                var bigImgSrc = objAbout.bigimg[0];
                var bigImgW = objAbout.bigimg[1];
                var bigImgH = objAbout.bigimg[2];
                
                var smImg = el.find('.kris-coll-img').html();
                bigImgBox.find('.insertBigimg').html(smImg);
                bigImgBox.find('.insertBigimg img').css({'width': bigImgW + 'px', 'height': bigImgH + 'px', 'opacity': 1});

                var bigImg = new Image();
                bigImg.src = bigImgSrc;


                if(bigImg.complete){ // if in cash
                    bigImgLoaded(); 
                }else { 
                    bigImg.onload = bigImgLoaded; 
                }

                function bigImgLoaded(){
                    bigImgBox.find('.insertBigimg img').attr('src', bigImgSrc).css('opacity', 0);
                    bigImgBox.find('.insertBigimg img').animate({opacity: 1}, 'fast');
                }

                var showMoreObj = bigImgBox.find('.insertShowMore');
                showMoreObj.data("type", objAbout.showmore[0]);
                showMoreObj.data("url", objAbout.showmore[1]);
                var showMoreVirtPr = bigImgBox.find('.txtPrm');
                showMoreVirtPr.data("isId", objAbout.id);
                var showMoreBtnBuy = bigImgBox.find('.btnBuy');
                showMoreBtnBuy.data("isId", objAbout.id);
                var showMoreBtnBuyImg = bigImgBox.find('.btnBuy');
                showMoreBtnBuyImg.data("isImg", currentSmImg);
                showMoreBtnBuyImg.data("isTit", objAbout.name);
                showMoreBtnBuyImg.data("isAutor", objAbout.autor);
                showMoreBtnBuyImg.data("isSmId", objAbout.id);
                showMoreObj.html('<img alt="" src="' + objAbout.showmore[2] + '">');
                var showMoreAddInFav = bigImgBox.find('.addlike');
                showMoreAddInFav.data("isFav", objAbout.fav);
                showMoreAddInFav.data("isFavId", objAbout.id);
            },
            clickBtnL: function(){
                $('.kris-coll-box').eq((currentOpen - 1)).click();
            },
            clickBtnR: function(){
                $('.kris-coll-box').eq((currentOpen + 1)).click();
            }
        };

        

        $element.init();
    }

    $.fn.detailinfo = function(options){
        return this.each(function(){
            if (undefined == $(this).data('detailinfo')){
                var plugin = new $.detailinfo(this, options);
                $(this).data('detailinfo', plugin);
            }
        });
    }
})(jQuery);























