(function($) {
    $.hints = function(element, options){
        var defaults = {
            txtInHint: 'Перейти&nbsp;в&nbsp;раздел'
        };
        var $element = $(element),
        element = element;
        var txtInHint;

        $element.init = function(){     
            this.settings = $.extend({}, defaults, options); 
            txtInHint = this.settings.txtInHint;

            $element.css({
                'position': 'relative'
            });

            var txtHint = $('<div class="iHint">'+txtInHint+'</div>');
            $element.prepend(txtHint);
            txtHint.css({
                'position': 'absolute'
            });
            $element.bind('mouseover', function(){
                $element.bind('mousemove', function(e){
                    var lPos = e.pageX - $element.offset().left;
                    var TPos = e.pageY - $element.offset().top - 40;

                    $element.find('.iHint').show();

                    $element.find('.iHint').css({
                        'left': lPos,
                        'top': TPos,
                    });
                    //console.log('hint');
                });
            });

            $element.bind('mouseout', function(){
                $('.iHint').hide();
            });
        };
        $element.init();
    }

    $.fn.hints = function(options){
        return this.each(function(){
            if (undefined == $(this).data('hints')){
                var plugin = new $.hints(this, options);
                $(this).data('hints', plugin);
            }
        });
    }
})(jQuery);

