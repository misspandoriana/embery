(function($) {
    $.favscr = function(element, options){
        var defaults = {};
        
        var $element = $(element),
        element = element;
        var likeCounter;
        var isFav;
        var isFavId;

        var clackFav = 0;


        $element.init = function(){     
            this.settings = $.extend({}, defaults, options); 

            $element.bind('click', clickAddFav);
            $('.favDell').live('click', clickRemFav);

            

            /*$('.addlike').mouseover(function(){
              isFav = $element.data('isFav');
              if(isFav == false){
                console.log('mouseover');
                $element.find('.imgFav').addClass('active');
              }
            });

            $('.addlike').mouseout(function(){
                isFav = $element.data('isFav');
              if(isFav == false){
                console.log('mouseout');
                $element.find('.imgFav').removeClass('active');
               }
            });*/
            var isOver = 0;

            $element.hover(   
                function(){
                    if(isOver == 0){
                        isOver = 1;
                        console.log('over');
                        $element.find('.imgFav').addClass('active');
                    }
                },
                function(){
                    if(!($element.hasClass('active'))){
                        console.log('out');
                        $element.find('.imgFav').removeClass('active');
                        isOver = 0;
                    }  
                }
            );
        };

        function clickRemFav(){
            $('.kris-likebtn').stop().animate({opacity: 0.5}, 'normal', function(){
                $('.kris-likebtn').stop().animate({opacity: 1}, 'normal');
            });
            isFav = $element.data('isFav');
            isFavId = $element.data('isFavId');
            if(isFav == true){
                //alert($element.find('.imgFav').attr('class'));
                $element.find('.imgFav').removeClass('active');
                $element.removeClass('active');
                console.log('removeClass');
                $element.find('.txtFav').html('Добавить в избранное');
                likeCounter = parseInt($('.likeCounter').html());
                likeCounter--;
                $('.likeCounter').html(likeCounter);
                $('.kris-coll-box.'+isFavId+'').data("about.fav", false);
                $element.data('isFav', false)
            }
        }

        function clickAddFav(){
            $('.kris-likebtn').stop().animate({opacity: 0.5}, 'fast', function(){
                $('.kris-likebtn').stop().animate({opacity: 1}, 'fast');
            });
            isFav = $element.data('isFav');
            isFavId = $element.data('isFavId');

            if(isFav == false){
                $element.find('.imgFav').addClass('active');
                $element.addClass('active');
                $element.find('.txtFav').html('В избранном&nbsp;&nbsp;<span class="favDell" style="font-size: 16px; text-decoration: underline;">Удалить</span>');
            
                likeCounter = parseInt($('.likeCounter').html());
                likeCounter++;
                $('.likeCounter').html(likeCounter);
                //alert($('.kris-coll-box.'+isFavId+'').data("about").fav);
                $('.kris-coll-box.'+isFavId+'').data("about.fav", true);
                //alert($('.kris-coll-box.'+isFavId+'').data("about.fav"));
                $element.data('isFav', true);
            }
            
     
            
        };



        

        $element.init();
    }

    $.fn.favscr = function(options){
        return this.each(function(){
            if (undefined == $(this).data('favscr')){
                var plugin = new $.favscr(this, options);
                $(this).data('favscr', plugin);
            }
        });
    }
})(jQuery);























