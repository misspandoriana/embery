(function($) {
    $.fadeBunner = function(element, options){
        var defaults = {};
        
        var $element = $(element),
        element = element;

        var imgItem = $element.find('.img-item');
        var titItem = $element.find('.tit-item');
        var indItem = $element.find('.ind-item');
        var goBoxFon = $element.find('.goBoxFon');
        var changeImgTimer;
        var elInd = 0;

        var mainElOfs = $element.offset().top;

        $element.init = function(){     
            this.settings = $.extend({}, defaults, options); 
            
            titItem.mouseover(
                function(){
                    //clearInterval(changeImgTimer);
                    var el = $(this);
                    elInd = el.index();
                    hoverOver();
                }
            );

            indItem.mouseover(
                function(){
                    //clearInterval(changeImgTimer);
                    var el = $(this);
                    elInd = el.index();
                    hoverOver();
                }
            );

            startImgTimer();

            $element.mouseover(function(){clearInterval(changeImgTimer)});

            $element.mouseout(function(){startImgTimer()});
            
        };

        var hoverOver = function(){
                var el = titItem.eq(elInd);
                if(elInd == 0){
                    var elOfT = el.offset().top - mainElOfs;
                }else{
                    var elOfT = el.offset().top - mainElOfs - 2;
                }
                
                goBoxFon.stop().animate({top: elOfT}, 250, 'swing');

                imgItem.stop().animate({'z-index': 12, 'opacity': 0}, 250);
                imgItem.eq(elInd).stop().animate({'z-index': 15, 'opacity': 1}, 250);

                indItem.removeClass('active');
                indItem.eq(elInd).addClass('active');
                titItem.removeClass('active');
                titItem.eq(elInd).addClass('active');
            };


            var startImgTimer = function(){
                changeImgTimer = setInterval(function(){funcImgTimer()}, 3000);
            };

            var funcImgTimer = function(){
                elInd++;
                if(elInd == 5){
                    elInd = 0;
                }
                //console.log('funcImgTimer elInd = ' + elInd);
                hoverOver();
            }

        $element.init();
    }

    $.fn.fadeBunner = function(options){
        return this.each(function(){
            if (undefined == $(this).data('fadeBunner')){
                var plugin = new $.fadeBunner(this, options);
                $(this).data('fadeBunner', plugin);
            }
        });
    }
})(jQuery);























