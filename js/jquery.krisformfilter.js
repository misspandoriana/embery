(function($) {
    $.krisformfilter = function(element, options){
        //qwe(element, options);
        
        var defaults = {
            deltaSelect:90,
            afterChoiceSel_2: function(i){}
		};
        
        var $element = $(element),
        element = element;
        var title = '';
        var firstval = '';
        var selectBox;
        var selscroll = 0;
        var deltaSel = 0;
        var flagscroll = 0;
        var coordfixscroll = 0;


        $element.init = function() {
                        
            this.settings = $.extend({}, defaults, options);
            deltaSel = this.settings.deltaSelect;
            afterChSel_2 = this.settings.afterChoiceSel_2;
            
            if($('.styleradio-1')){checker('.styleradio-1')};
            if($('.styleradio-2 input')){checker('.styleradio-2 input')};
            
            if($('.stylecheckbox-1')){checker('.stylecheckbox-1')};
            
            if($('.styleselect-2')){checker('.styleselect-2')};
            
            // styletoddler-1 zadaetsya y blocka v kotoriy nado pomestit polzunok
            if($('.styletoddler-1')){$element.toddlerfunction('.styletoddler-1')};
            

        };
        
        function checker(workclass){
            $.each($(workclass), function(i, obj){
                if(obj.tagName.toLowerCase()=='input'){
                    if(workclass == '.styleradio-1' || workclass == '.styleradio-2 input'){
                        if($(this).attr('type')=='radio'){
                            if(workclass == '.styleradio-1'){
                                $element.radiofunction(obj, 'style-1');
                            }else if(workclass == '.styleradio-2 input'){
                                $element.radiofunction(obj, 'style-2');
                            }
                        }else{
                            alert('Ошибка ' + workclass + ': Неверный тип input! Требуется тип radio')   
                        }
                    }else if(workclass == '.stylecheckbox-1'){
                        if($(this).attr('type')=='checkbox'){
                            $element.checkboxfunction(obj);
                        }else{
                            alert('Ошибка ' + workclass + ': Неверный тип input! Требуется тип checkbox')   
                        }
                    }
                }else if(obj.tagName.toLowerCase()=='select'){
                    if(workclass == '.styleselect-2'){
                        $element.selectfunction(obj, 'style-2');
                    }
                }else{
                    alert('Ошибка ' + workclass + ': Неверный элемент!')   
                };
            });
        };
        
        
        $element.selectfunction = function(obj, style){
            if(style == 'style-2'){
                var obj = $(obj);
                var selectBox = $('<div>').addClass('kris_selektbox_t2');
                obj.hide().after(selectBox);
                var tit = $('<div class="selektbox-title-t1 passive"></div>');          
                selectBox.append(tit);        
                    var selectList = $('<div class="kris-listbox"></div>');
                        var selbody = $('<div class="kris-selbody"></div>');
                            var selbodyrels = $('<div class="kris-selbody-rels"></div>');
                                var selbody_ul = $('<ul class="kris-selbody__ul"></ul>');
                        var scrollbox = $('<div class="kris_scrollbox"></div>');
                            var scroll = $('<div class="kris_scroll"></div>');                            
    
                  selectBox.append(selectList);
                    selectList.append(selbody);
                    selbody.append(selbodyrels);
                        selbodyrels.append(selbody_ul);
                  selectList.append(scrollbox);
                    scrollbox.append(scroll); 

                 
                 var z = parseInt(selectBox.css("z-index"));
                 
                 tit.bind("click",{zindex : z}, function(event){
                    $('.kris_selektbox_t2').css({"z-index": event.data.zindex});
                    selectBox.css({"z-index":(event.data.zindex + 1)});
                    if(selectList.is(":hidden")){
                        $('.kris-listbox').slideUp("normal", "swing", function(){$('.kris-selbody__ul').css({"top":0}); $('.kris_scroll').css({"top":0});});
                        $('.kris_title').removeClass('active');
                        $('.kris_title').addClass('passive');
                        selectList.slideDown({duration: 1000, easing:"easeOutBounce"});  
                        tit.removeClass('passive');
                        tit.addClass('active');
                    }else{
                        selectList.slideUp("normal", "swing", function(){selbody_ul.stop().css({"top":0}); scroll.css({"top":0});}); 
                        tit.removeClass('active');
                        tit.addClass('passive');
                    }
                    return false;
                 });

                obj.find('option').each(function(i, opt){
                 
                        var li = libuild(opt);
                        function libuild(opt){
                            return $('<li class="kris-selbody__ul__li">').append('<span>'+opt.text+'</span>');
                         };
                        
                        selbody_ul.append(li);
                        li.click(function(){
                            tit.html('<span class="selektbox-title-t1__span">'+li.text()+'</span><div class="selektbox-title-t1__btn"><div class="selektbox-title-t1__btn2"></div></div>');

                            selectList.slideUp("normal", "swing");
                            tit.removeClass('active');
                            tit.addClass('passive');
                            obj.find('option').removeAttr('selected').eq(i).attr('selected', true);
                            var actopt = obj.find('option').eq(i);
                            $(li).selectbind({actp:actopt});
                            
                            
                            
                            return false;
                        })

                    if(i == 0){
                        firstval = opt.text;
                    }

                    if(i == (obj.find('option').length - 1)){
                            tit.html('<span class="selektbox-title-t1__span">'+firstval+'</span><div class="selektbox-title-t1__btn"><div class="selektbox-title-t1__btn2"></div></div>');
                        }
    
                 });
    
                 
                 
                 $(document).click(function(event){
                    if(selectList.is(":visible")){
                        selectList.slideUp("normal", "swing");
                        $('.selektbox-title-t1').removeClass('active');
                        $('.selektbox-title-t1').addClass('passive');
                        return false;
                    }                
                 });

                 var ulheight = selbody_ul.height();
                 

                 var ulparrent = selbodyrels.height();
                 selectList.hide();
                 
                 if(ulheight > ulparrent){
                    scrollbox.click(function(event){
                        return false;           
                    });
                    var scrollY;
                    var contentY;
                    var scrollboxheight = scrollbox.height();
                    var scrollheight = scroll.height();
                    var scrollrels = scrollboxheight-scrollheight;
                    selectList.css({"width": (selbody.width() + scrollbox.width())});
                    scrollbox.show();

                    scroll.bind('mousedown', function(e){
                        flagscroll = 1;
                        $(document).bind('mousemove', function(e){
                            if(flagscroll == 1){
                                coordfixscroll = e.pageY - scrollbox.offset().top - 20;
                                scrollY = parseInt(scroll.css('top'));
                                if(scrollY >= 0 && scrollY <= scrollrels){
                                    scroll.css('top', coordfixscroll);
                                }
                                if(parseInt(scroll.css('top')) < 0){
                                    scroll.css('top', 0);
                                }
                                if(parseInt(scroll.css('top')) > scrollrels){
                                    scroll.css('top', scrollrels);
                                }
                                contentY = -(selbody_ul.height() - selbodyrels.height())* scrollY/scrollrels; 
                                selbody_ul.css('top', contentY);
                            };
                        });
                        return false;
                    }); 
    
                    $(document).bind('mouseup', function(e){
                        flagscroll = 0;
                    });
                    selbodyrels.mousewheel(function(event, delta){
                        event.preventDefault(); 
                        if(delta > 0){
                            moveBot();
                        }
                        if(delta < 0){
                            moveTop();
                        } 
                    });
                }else{
                    selectList.css({"width": selbody.width()});
                    selbodyrels.css({"height": ulheight});
                    scrollbox.hide();
                }
                function moveTop(){
                    var ultop = parseInt(selbody_ul.css("top"));
                    var ulheight = selbody_ul.height();
                    var ulparrent = selbodyrels.height();
                    var scrollboxheight = scrollbox.height();
                    var scrollheight = scroll.height();
                    var scrollrels = scrollboxheight-scrollheight;
                    var topcoord = ulparrent - ulheight;
                    if((selscroll == 0) && (ultop > topcoord)){
                        if((ultop - deltaSel) > topcoord){
                            selscroll = 1;
                            selbody_ul.animate({"top":"-="+deltaSel+"px"}, "slow", function(){selscroll = 0;});
                            var selY = -(ultop - deltaSel)*scrollrels/(ulheight - ulparrent);
                            scroll.animate({"top":selY}, "slow");
                        }else{
                            selscroll = 1;
                            selbody_ul.animate({"top":topcoord}, "slow", function(){selscroll = 0;});
                            var selY = -topcoord*scrollrels/(ulheight - ulparrent);
                            scroll.animate({"top":selY}, "slow"); 
                        }
                    }
                };
                function moveBot(){
                    var ultop = parseInt(selbody_ul.css("top"));
                    var ulheight = selbody_ul.height();
                    var ulparrent = selbodyrels.height();
                    var scrollboxheight = scrollbox.height();
                    var scrollheight = scroll.height();
                    var scrollrels = scrollboxheight-scrollheight;
                    var topcoord = ulparrent - ulheight;
                    if(selscroll == 0 && ultop < 0){
                        if((ultop + deltaSel) < 0){
                            selscroll = 1;
                            selbody_ul.animate({"top":"+="+deltaSel+"px"}, "slow", function(){selscroll = 0;});
                            var selY = -(ultop + deltaSel)*scrollrels/(ulheight - ulparrent);
                            scroll.animate({"top":selY}, "slow");   
                        }else{
                            selscroll = 1;
                            selbody_ul.animate({"top":0}, "slow", function(){selscroll = 0;});
                            scroll.animate({"top":0}, "slow"); 
                        }
                    }
                };
                
                             
            }
                                    
        };
        
        $element.toddlerfunction = function(workclass){
            $.each($(workclass), function(i, obj){
                var obj = $(obj);
                if(obj.data("name1")&& obj.data("name2") && obj.data("end") && obj.data("step")){
                if((obj.data("start") != 'undefined') || (obj.data("start") != '')){   
                    var inputstart = $('<input type="hidden" name="'+obj.data("name1")+'" value="'+obj.data("start")+'"/>');
                    var inputend = $('<input type="hidden" name="'+obj.data("name2")+'" value="'+obj.data("end")+'"/>');
                    var toddlerBox = $('<div>').addClass('kris_toddler_t1');
                        var btnLeft = $('<div>').addClass('kris_toddler_btn toddler_btnleft');
                        var toddlerActive = $('<div>').addClass('kris_toddler_active');
                        var btnRight = $('<div>').addClass('kris_toddler_btn toddler_btnright');
                    obj.append(inputstart);
                    obj.append(inputend);
                    obj.append(toddlerBox);
                        toddlerBox.append(btnLeft);
                        toddlerBox.append(toddlerActive);
                        toddlerBox.append(btnRight);

                    var flagbtnL;
                    var btnXL; 
                    var flagbtnR;
                    var btnXR;
                    var btnWidth = $('.kris_toddler_btn').outerWidth();
                    var actWidth = toddlerActive.width();
                    var leftValue = obj.data("start");
                    var rightValue = obj.data("end");
                    var step = (actWidth*obj.data("step"))/((rightValue - leftValue) - 1);// -1 tak kak poslednee znachenie y nas pokazivaet vtoroi polzunok
                    btnLeft.text(leftValue);
                    btnRight.text(rightValue);
                    var leftCoun = 0;
                    var left;
                    var right;                                        
                    
                    function leftCoord(btn){
                        var lc = parseInt(btn.css('left'));
                        return lc;
                    };

                    btnLeft.bind('mousedown', function(e){
                        flagbtnL = 1;
                        var actWidthPoint = toddlerActive.width();
                        var actLeftPos = leftCoord(toddlerActive);
                        
                        $(document).bind('mousemove', function(e){
                            if(flagbtnL == 1){
                                coordbtn = e.pageX - toddlerBox.offset().left - 20;
                                btnXL = leftCoord(btnLeft);
                                if(btnXL >= 0 && btnXL <= (leftCoord(btnRight) - btnWidth)){
                                    btnLeft.css('left', coordbtn);
                                    toddlerActive.css({left:(coordbtn + btnWidth), width:(actWidthPoint -((coordbtn + btnWidth) - actLeftPos))});
                                    left = Math.round(coordbtn/step) + leftValue;
                                    if((left > leftValue) && (left < rightValue)){                                                                        
                                        btnLeft.text(left);                                     
                                        inputstart.val(left);        
                                    }   
                                }
                                if(leftCoord(btnLeft) < 0){
                                    btnLeft.css('left', 0);
                                    toddlerActive.css({left:btnWidth, width:(actWidthPoint -(btnWidth - actLeftPos))});
                                    left = leftValue;                                    
                                    btnLeft.text(left);
                                    inputstart.val(left);                                     
                                }
                                if(leftCoord(btnLeft) > (leftCoord(btnRight) - btnWidth)){
                                    btnLeft.css('left', (leftCoord(btnRight) - btnWidth));
                                    toddlerActive.css({left:(leftCoord(btnLeft) + btnWidth), width:0});
                                    left = (Math.round((leftCoord(btnRight) - btnWidth)/step) + leftValue);                                   
                                    btnLeft.text(left); 
                                    inputstart.val(left);                                          
                                }
                            };
                        });
                        return false;
                    }); 
                    
                    btnRight.bind('mousedown', function(e){
                        flagbtnR = 1;
                        var actWidthPoint = toddlerActive.width();
                        var actRightPos = leftCoord(btnRight);
                        var rz = btnRight.text();
                        $(document).bind('mousemove', function(e){
                            if(flagbtnR == 1){
                                coordbtn = e.pageX - toddlerBox.offset().left - 20;
                                btnXR = leftCoord(btnRight);
                                if(btnXR >= (leftCoord(btnLeft) + btnWidth) && btnXR <= (toddlerBox.width() - btnWidth)){
                                    btnRight.css('left', coordbtn);
                                    toddlerActive.css({width:(actWidthPoint - (actRightPos-coordbtn))});
                                    right = rz - (Math.round((actRightPos-coordbtn)/step) + leftValue);
                                    if((right < rightValue) && (right > leftValue)){                                                                        
                                        btnRight.text(right);  
                                        inputend.val(right); 
                                    }                                                                        
                                }
                                if(leftCoord(btnRight) < (leftCoord(btnLeft) + btnWidth)){
                                    btnRight.css('left', (leftCoord(btnLeft) + btnWidth));
                                    toddlerActive.css({width:0});
                                    right = rz - (Math.round((actRightPos-(leftCoord(btnLeft) + btnWidth))/step) + leftValue);                                    
                                    btnRight.text(right);  
                                    inputend.val(right); 
                                }
                                if(leftCoord(btnRight) > (toddlerBox.width() - btnWidth)){
                                    btnRight.css('left', (toddlerBox.width() - btnWidth));
                                    toddlerActive.css({width:(actWidthPoint - (actRightPos-(toddlerBox.width() - btnWidth)))});
                                    right = rightValue;                                    
                                    btnRight.text(right);
                                    inputend.val(right);                                    
                                }
                            };
                        });
                        return false;
                    }); 
    
                    $(document).bind('mouseup', function(e){
                        flagbtnL = 0;
                        flagbtnR = 0;
                    });
                    }
                }else{
                    alert('Ошибка ' + workclass + ': Задайте name[], start, end, step')
                }
            });            
        };

        $element.checkboxfunction = function(obj, style){
            var obj = $(obj);
            if(obj.data("label")){var txt = obj.data("label")}else{var txt = ''};
            var checkboxBox = $('<div>').addClass('kris_checkbox_t1');
            var checkboxBody = $('<div class="krisform_checkbox">' + txt + '</div>');
            obj.wrap(checkboxBox);
            obj.hide().after(checkboxBody);
            if(obj.is(':checked')){
                checkboxBody.addClass('checkbox_active');
            }
            if(obj.is(':disabled')){
                checkboxBody.addClass('checkbox_passive');
            }
            checkboxBody.parent().click(function(){
                if(!checkboxBody.is('.checkbox_passive')){
                    if (obj.is(':checked')) {
		  					obj.removeAttr('checked');
		  					checkboxBody.removeClass('checkbox_active');
                            $(this).removeClass('cliked');
	  	  				} else {
		  					obj.attr('checked', true);
		  					checkboxBody.addClass('checkbox_active');
                            $(this).addClass('cliked');
		  				}
                    return false;  
                };
            }); 
            checkboxBody.hover(   
                function(){
                    $(this).addClass('checkbox_active');
                },
                function(){
                    if(!(checkboxBody.parent().hasClass('cliked'))){
                        $(this).removeClass('checkbox_active');
                    }  
                }
            );
        };

        $element.radiofunction = function(obj, style){
            var obj = $(obj);
            if(obj.data("label")){var txt = obj.data("label")}else{var txt = ''};
            
            if(style == 'style-1'){
                var radioBox = $('<div>').addClass('kris_radio_t1');
                var radioBody = $('<span class="krisform-radio"></span>');
                var radioTitle = $('<span class="radio-title">' + txt + '</span>');
            }else if(style == 'style-2'){
                var radioBox = $('<div>').addClass('kris_radio_t2');
                var radioBody = $('<span class="krisform-radio">' + txt + '</span>');
            };
            
            obj.wrap(radioBox);
            obj.hide().after(radioBody);
            if(radioTitle){radioBody.after(radioTitle)};
            if(obj.is(':checked')){
                radioBody.addClass('radio-active');
            };
            if($element.is(':disabled')){
                radioBody.addClass('radio-passive');
            };
            radioBody.parent().click(function(){
                if(!radioBody.is('.radio-passive')){
                    if (obj.is(':checked')){
                        $('input[name="'+obj.attr('name')+'"]').next().removeClass('radio-active');
                        obj.attr('checked', false);
                        $(this).removeClass('cliked');
                    }else{
                        $('input[name="'+obj.attr('name')+'"]').attr('checked', false).next().removeClass('radio-active');
                        $('.label').removeClass('cliked');
                        obj.attr('checked', true);
                        radioBody.addClass('radio-active');
                        $('.'+$(this).attr('class')+'.cliked').removeClass('cliked');
                        $(this).addClass('cliked');
                    }
                    return false;  
                };
            });
        };
        $element.init();
    }

    $.fn.krisformfilter = function(options){
        return this.each(function(){
            if (undefined == $(this).data('krisformfilter')){
                var pluginkrisformfilter = new $.krisformfilter(this, options);
                $(this).data('krisformfilter', pluginkrisformfilter);
            }
        });
    }
})(jQuery);























