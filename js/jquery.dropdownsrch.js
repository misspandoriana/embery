(function($) {
    $.dropdownsrch = function(element, options){
        var defaults = {};
        
        var $element = $(element),
        element = element;
        var drdSrchBox = $('.kris-drdSrchBox');
        var srchInput = $('.kris-srch-input');
        var closeBtn = $('.kris-drdSrch-close');

        var flag = 0;
        var isVisible = 0;



        $element.init = function(){     
            this.settings = $.extend({}, defaults, options); 
            srchInput.bind('textchange', function (event, previousText) {
                if(flag == 0){
                    var isValL = srchInput.val().length;
                    if(isValL >= 2){
                        if(isVisible == 0){
                            isVisible = 1;
                            drdSrchBox.show();
                        }
                    }else{
                        if(isVisible == 1){
                            drdSrchBox.hide();
                            isVisible = 0;
                        };
                    }
                }
            });

            drdSrchBox.click(function(){
                return false;
            });
        };

        $(document).click(function(event){
            if(isVisible == 1){
                drdSrchBox.hide();
                isVisible = 0;
            };
            
        });

        closeBtn.click(function(event){
            flag = 1;
            drdSrchBox.hide();
        });

        

        $element.init();
    }

    $.fn.dropdownsrch = function(options){
        return this.each(function(){
            if (undefined == $(this).data('dropdownsrch')){
                var plugin = new $.dropdownsrch(this, options);
                $(this).data('dropdownsrch', plugin);
            }
        });
    }
})(jQuery);























