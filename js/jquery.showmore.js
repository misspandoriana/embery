(function($) {
    $.showmore = function(element, options){
        var defaults = {
            tmplformat: 'isType1'
        };
        
        var $element = $(element),
        element = element;

        var typeInfo;
        var urlInfo;
        var idInfo;
        var idBuy;
        var smImg;
        var smImgTit;
        var smImgAutor;
        var smImgId;


        var tmplFormat;

        var fonbox;
        var showBody;
        var showClose;
        var showBox;
        var showInfo;

        var showInfoTb;
        var showInfoVa;
        var buySmImg;

        var boxW;    
        var boxH;  

        var winW;
        var winH;

        var btnMore = $element;


        $element.init = function(){     
            this.settings = $.extend({}, defaults, options); 
            tmplFormat = this.settings.tmplformat;
            createCarcas();

        };


        $(window).scroll(function(){
          //var currentScrollTop = $(window).scrollTop();
          if(showBody.is(":visible")){
            calcSizePos();
          }
      });

        function calcSizePos(){
            boxW = showBody.width();    
            boxH = showBody.height();  
            winW = $(window).width();
            winH = $(window).height();
            var margL = (winW - boxW)/2 + $(window).scrollLeft();
            var margT = (winH - boxH)/2 + $(window).scrollTop();
            showBody.css({'left': margL + 'px','top': margT + 'px'});
        }

        function createCarcas(){
            fonbox = $('<div class="kris-fonbox"></div>');
            if(tmplFormat == 'isType1'){
                showBody = $('<div class="kris-showBody isType1"></div>');
                    showClose = $('<div class="kris-showClose"></div>');
                    showBox = $('<div class="kris-showBox isType1"></div>');
                        showInfo = $('<div class="kris-showInfo isType1"></div>');
            }else if(tmplFormat == 'isType2'){
                showBody = $('<div class="kris-showBody isType2"></div>');
                    showClose = $('<div class="kris-showClose"></div>');
                    showBox = $('<div class="kris-showBox isType2"></div>');
                        showInfo = $('<div class="kris-showInfo isType2"></div>');
            }else if(tmplFormat == 'isType3'){
                showBody = $('<div class="kris-showBody isType3"></div>');
                    showClose = $('<div class="kris-showClose"></div>');
                    showBox = $('<div class="kris-showBox isType3"></div>');
                        showInfo = $('<div class="kris-showInfo isType3"></div>');
            }else if(tmplFormat == 'isType4'){
                showBody = $('<div class="kris-showBody isType4"></div>');
                    showClose = $('<div class="kris-showClose"></div>');
                    showBox = $('<div class="kris-showBox isType4"></div>');
                        showInfo = $('<div class="kris-showInfo isType4"></div>');
            }else if(tmplFormat == 'isType5'){
                showBody = $('<div class="kris-showBody isType5"></div>');
                    showClose = $('<div class="kris-showClose"></div>');
                    showBox = $('<div class="kris-showBox isType5"></div>');
                        showInfo = $('<div class="kris-showInfo isType5"></div>');
            }else if(tmplFormat == 'isType6'){
                showBody = $('<div class="kris-showBody isType6"></div>');
                    showClose = $('<div class="kris-showClose"></div>');
                    showBox = $('<div class="kris-showBox isType6"></div>');
                        showInfo = $('<div class="kris-showInfo isType6"></div>');
            }else if(tmplFormat == 'isType7'){
                showBody = $('<div class="kris-showBody isType7"></div>');
                    showClose = $('<div class="kris-showClose"></div>');
                    showBox = $('<div class="kris-showBox isType7"></div>');
                        showInfo = $('<div class="kris-showInfo isType7"></div>');
            }else if(tmplFormat == 'isType8'){
                showBody = $('<div class="kris-showBody isType8"></div>');
                    showClose = $('<div class="kris-showClose"></div>');
                    showBox = $('<div class="kris-showBox isType8"></div>');
                        showInfo = $('<div class="kris-showInfo isType8"></div>');
            }else if(tmplFormat == 'isType9'){
                showBody = $('<div class="kris-showBody isType9"></div>');
                    showClose = $('<div class="kris-showClose"></div>');
                    showBox = $('<div class="kris-showBox isType9"></div>');
                        showInfo = $('<div class="kris-showInfo isType9"></div>');

                setInterval(hideMoreInfo, 3000); 
    

            }
            

            $('body').append(fonbox);
            $('body').append(showBody);
                showBody.append(showClose);
                showBody.append(showBox);
                    showBox.append(showInfo);


            if(tmplFormat == 'isType1'){
                tmplT1(); 
            }else if(tmplFormat == 'isType2'){
                tmplT2(); 
            }else if(tmplFormat == 'isType3'){
                tmplT3(); 
            }else if(tmplFormat == 'isType4'){
                tmplT4(); 
            }else if(tmplFormat == 'isType5'){
                tmplT5(); 
            }else if(tmplFormat == 'isType6'){
                tmplT6(); 
            }else if(tmplFormat == 'isType7'){
                tmplT7(); 
            }else if(tmplFormat == 'isType8'){
                tmplT8(); 
            }else if(tmplFormat == 'isType9'){
                tmplT9(); 
            };          

            btnMore.bind('click', function(){
                if((tmplFormat != 'isType7') && (tmplFormat != 'isType9')){
                    if(tmplFormat == 'isType1'){
                        typeInfo =  $(this).data('type');
                        urlInfo =  $(this).data('url');
                        contentT1(typeInfo, urlInfo);   
                    }else if(tmplFormat == 'isType4'){
                        idInfo =  $(this).data('isId');
                        contentT4(idInfo);  
                    }else if(tmplFormat == 'isType5'){
                        idBuy =  $(this).data('isId');
                        smImg = $(this).data('isImg');
                        smImgTit = $(this).data('isTit');
                        smImgAutor = $(this).data('isAutor');
                        smImgId = $(this).data('isSmId');
                        contentT5(idBuy);  
                    };
                    
                    showMoreInfo();
                }
                
                
            });

            fonbox.bind('click', function(){
                hideMoreInfo();
            });

            showClose.bind('click', function(){
                hideMoreInfo();
            });
        };

        

        function showMoreInfo(typeInfo, urlInfo){
                calcSizePos();
                fonbox.show();
                showBody.show();
                fonbox.animate({'opacity': 1});
                showBody.animate({'opacity': 1});
            };

            function hideMoreInfo(){
                fonbox.animate({'opacity': 0}, function(){fonbox.hide();});
                if(tmplFormat == 'isType1'){
                    if(typeInfo == 'pic'){
                        showBody.animate({'opacity': 0}, function(){showBody.hide(); showBody.find('img').remove()});
                    }else if(typeInfo == 'video'){
                        showBody.animate({'opacity': 0}, function(){showBody.hide(); showBody.find('iframe').remove()});
                    }
                }else if(tmplFormat == 'isType2'){
                    showBody.animate({'opacity': 0}, function(){showBody.hide();});
                }else if(tmplFormat == 'isType3'){
                    showBody.animate({'opacity': 0}, function(){showBody.hide();});
                }else if(tmplFormat == 'isType4'){
                    showBody.animate({'opacity': 0}, function(){showBody.hide();});
                }else if(tmplFormat == 'isType5'){
                    showBody.animate({'opacity': 0}, function(){showBody.hide();});
                }else if(tmplFormat == 'isType6'){
                    showBody.animate({'opacity': 0}, function(){showBody.hide();});
                }else if(tmplFormat == 'isType7'){
                    showBody.animate({'opacity': 0}, function(){showBody.hide();});
                }else if(tmplFormat == 'isType8'){
                    showBody.animate({'opacity': 0}, function(){showBody.hide();});
                }else if(tmplFormat == 'isType9'){
                    showBody.animate({'opacity': 0}, function(){showBody.hide();});
                }; 
            };

            function tmplT1(){
                showInfoVa = $('<div class="showInfoVa"></div>');
                    showInfo.append(showInfoVa);
            };

            function contentT1(){
                if(typeInfo == 'pic'){
                    var contentBox = $('<img alt="" src="' + urlInfo + '"/>');
                }else if(typeInfo == 'video'){
                    var contentBox = $('<iframe width="640" height="390" src="' + urlInfo + '" frameborder="0" allowfullscreen></iframe>');
                    
                }
                showInfoVa.append(contentBox);
            };

            function contentT4(idInfo){
                $('.isIdPic').val(idInfo);
            };
            function contentT5(idBuy){
                $('.isIdPicBuy').val(idBuy);
                buySmImg = $('<img alt="" src="'+smImg+'"/>');
                $('.isTpl5').find('.kris-sm-img').html('').append(buySmImg);
                $('.isTpl5').find('.tit').html('').append(smImgTit);
                $('.isTpl5').find('.id').html('ID: ').append(smImgId);
                $('.isTpl5').find('.autor').html('').append(smImgAutor);
            };

            function tmplT2(){
                $('.isTpl2').detach().prependTo(showInfo);
            };

            function tmplT3(){
                $('.isTpl3').detach().prependTo(showInfo);
               
            };
            function tmplT4(){
                $('.isTpl4').detach().prependTo(showInfo);
               
            };
            function tmplT5(){
                $('.isTpl5').detach().prependTo(showInfo);
               
            };
            function tmplT6(){
                $('.isTpl6').detach().prependTo(showInfo);
               
            };

            function tmplT7(){
                $('.isTpl7').detach().prependTo(showInfo);
                showMoreInfo();
               
            };

            function tmplT8(){
                $('.isTpl8').detach().prependTo(showInfo);               
            };

            function tmplT9(){
                $('.isTpl9').detach().prependTo(showInfo);
                showMoreInfo();
               
            };


        

        $element.init();
    }

    $.fn.showmore = function(options){
        return this.each(function(){
            if (undefined == $(this).data('showmore')){
                var plugin = new $.showmore(this, options);
                $(this).data('showmore', plugin);
            }
        });
    }
})(jQuery);























