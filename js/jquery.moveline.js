(function($) {
    $.moveline = function(element, options){
        var defaults = {};
        
        var $element = $(element),
        element = element;


        var leftBtn = $element.find('.leftBtn');
        var rightBtn = $element.find('.rightBtn');
        var parrent = $element.find('.parrent');
        var parrentW = $element.find('.parrent').width();
        var itemCount = $element.find('.parrent .item').length;
        var itemW = $element.find('.parrent .item').outerWidth();
        var itemML = parseInt($element.find('.parrent .item').last().css('margin-left'));
        var rels = $element.find('.rels');
        var relsW = (itemW * itemCount) + ((itemCount - 1) * itemML);
        var speed = 3000;
       // console.log('speed = ' + speed);
        



        $element.init = function(){     
            this.settings = $.extend({}, defaults, options); 
            
            $element.find('.rels').width(relsW);    

            if(relsW <= parrentW){
                leftBtn.css({'visibility':'hidden'});
                rightBtn.css({'visibility':'hidden'});
            }

            var relsLeftpPos = parseInt(rels.css('left'));
            if(relsLeftpPos == 0){
                leftBtn.css({'visibility':'hidden'});
            }
            if(relsLeftpPos == (-(relsW - parrentW))){
                rightBtn.css({'visibility':'hidden'});
            }


            leftBtn.bind('click', clickLeftBtn);

            rightBtn.bind('click', clickRightBtn);

            var itemEl = $element.find('.parrent .item');

            $.each(itemEl, function(index, item){
                var innerImg = $(this).find('img');
                var innerAhr = $(this).find('a').attr('href');
                var innerAtit = $(this).find('a').attr('title');
                var srcImg = innerImg.attr('src');
                var isImg = new Image();
                isImg.src = srcImg;



                var isImgMaxWStart = parseInt(innerImg.css('maxWidth'));
                var isImgMaxHStart = parseInt(innerImg.css('maxHeight'));

                var bigImg = $('<a class="bigInnerImg" href="'+innerAhr+'"><img src="'+isImg.src +'"/></a>');
                var txtHint = $('<div class="hintML '+index+'">'+innerAtit+'</div>');
                $element.prepend(bigImg);
                $element.prepend(txtHint);
                //$(item).bind('mouseover', itemOver);

            });

            

        };

        var clickLeftBtn = function(){
            var relsLeftpPos = parseInt(rels.css('left'));
            var delta = (-relsLeftpPos);
            rightBtn.css({'visibility':'visible'});

            if(delta <= parrentW){
                var newRelsLeftpPos = 0;
                leftBtn.css({'visibility':'hidden'});
                var cnt = delta/(parrentW + itemML);
                var sp = speed * cnt;
                //console.log('sp = ' + sp);
            }else{
                var newRelsLeftpPos = (relsLeftpPos + parrentW + itemML); 
                var sp = speed;
            }
            rels.stop().animate({'left':newRelsLeftpPos}, sp, 'linear');
        };

        var clickRightBtn = function(){
            var relsLeftpPos = parseInt(rels.css('left'));
            var delta = relsW - (-relsLeftpPos) - parrentW;
            leftBtn.css({'visibility':'visible'});

            if(delta <= parrentW){
                var newRelsLeftpPos = -(relsW - parrentW);
                rightBtn.css({'visibility':'hidden'});

                var cnt = delta/(parrentW + itemML);
                var sp = speed * cnt;
               // console.log('sp = ' + sp);
                
            }else{
                var newRelsLeftpPos = (relsLeftpPos - parrentW - itemML);   
                var sp = speed;
            }
            rels.stop().animate({'left':newRelsLeftpPos}, sp, 'linear');
        };

        
        $element.init();
    }

    $.fn.moveline = function(options){
        return this.each(function(){
            if (undefined == $(this).data('moveline')){
                var plugin = new $.moveline(this, options);
                $(this).data('moveline', plugin);
            }
        });
    }
})(jQuery);























