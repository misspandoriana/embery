(function($) {
    $.advsrch = function(element, options){
        var defaults = {};
        
        var $element = $(element),
        element = element;



        var slideUpBtn = $element.find('.advsrchclose');
        var slideDownBtn = $element.find('.kris-advsrchbox');
        var togleBox = $element.find('.advsrchbox');

        var isVisible = 0;


        $element.init = function(){     
            this.settings = $.extend({}, defaults, options); 

            togleBox.bind('click', function(){return false;});

            slideUpBtn.bind('click', function(){
                isVisible = 0;
                togleBox.slideUp('normal', function(){slideDownBtn.removeClass('active');});
            });

            slideDownBtn.click(function(){
                if(togleBox.is(":visible")){
                    isVisible = 0;
                    togleBox.slideUp('normal', function(){slideDownBtn.removeClass('active');});
                }else{
                    slideDownBtn.toggleClass("active");
                    togleBox.slideDown('normal', function(){isVisible = 1;});
                }
            });


        };
        $('.kris-advsrch-submit').click(function(){
            isVisible = 0;
            togleBox.slideUp('normal', function(){slideDownBtn.removeClass('active');});
        });

        $(document).click(function(event){
            if(isVisible == 1){
                //console.log('isVisible true= ' + isVisible);
                isVisible = 0;
                togleBox.slideUp('normal', function(){slideDownBtn.removeClass('active');});
            }
            //else{
               // console.log('isVisible false= ' + isVisible);
            //}   
           // return false;             
         });

        $element.init();
    }

    $.fn.advsrch = function(options){
        return this.each(function(){
            if (undefined == $(this).data('advsrch')){
                var plugin = new $.advsrch(this, options);
                $(this).data('advsrch', plugin);
            }
        });
    }
})(jQuery);























